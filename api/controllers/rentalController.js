'use strict';


var mongoose = require('mongoose'),
Rental = mongoose.model('Rentals');

exports.list_all_rentals = function(req, res) {
  Rental.find({}, function(err, rnt) {
    if (err)
      res.send(err);
    res.json(rnt);
  });
};


exports.create_a_rental = function(req, res) {
  var new_rental = new Rental(req.body);
  new_rental.save(function(err, rnt) {
    if (err)
      res.send(err);
    res.json(rnt);
  });
};


exports.read_a_rental = function(req, res) {
  Rental.findOne(req.params.clientId, function(err, rnt) {
    if (err)
      res.send(err);
    res.json(rnt);
  });
};


exports.update_a_rental = function(req, res) {
  Rental.findOneAndUpdate({clientId: req.params.clientId}, req.body, {new: true}, function(err, rnt) {
    if (err)
      res.send(err);
    res.json(rnt);
  });
};


exports.delete_a_rental = function(req, res) {


  Rental.remove({
    clientId: req.params.clientId
  }, function(err, rnt) {
    if (err)
      res.send(err);
    res.json({ message: 'Rental successfully deleted' });
  });
};