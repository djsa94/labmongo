'use strict';


var mongoose = require('mongoose'),
  Car = mongoose.model('Cars');

exports.list_all_cars = function(req, res) {
  Car.find({}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};


exports.create_a_car = function(req, res) {
  var new_car = new Car(req.body);
  new_car.save(function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};


exports.read_a_car = function(req, res) {
  Car.find({'licensePlate' : req.params.licensePlate}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};



exports.read_cars_by_brand = function(req, res) {
  Car.find({'brandId' : req.params.brandId}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};

exports.read_cars_by_price_range = function(req, res) {
  Car.find({'dailyCost' :  {"$lt" : parseInt(req.body.topPrice)+1, "$gt" : parseInt(req.body.botPrice)-1 }}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};
exports.read_cars_by_model = function(req, res) {
  Car.find({'carModel' : req.body.carModel}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};

exports.update_a_car = function(req, res) {
  Car.findOneAndUpdate({licensePlate: req.params.licensePlate}, req.body, {new: true}, function(err, car) {
    if (err)
      res.send(err);
    res.json(car);
  });
};


exports.delete_a_car = function(req, res) {


  Car.remove({
    licensePlate: req.params.licensePlate
  }, function(err, car) {
    if (err)
      res.send(err);
    res.json({ message: 'Car successfully deleted' });
  });
};