'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ClientSchema = new Schema({
  clientId: {
    type: String,
    required: 'Please enter an ID'
  },
  fullName: {
    type: String,
    required: 'Please enter client name'
  },
  email:{
    type: String,
    required: 'Please enter client email'
  },
  phoneNumber:{
    type: Number,
    required: 'Please enter phone number'
  }
});

module.exports = mongoose.model('Clients', ClientSchema);