'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CarSchema = new Schema({
  licensePlate: {
    type: String,
    required: 'Please enter licensePlate number'
  },
  capacity: {
    type: Number
  },
  brandId: {
    type: Number,
    required: 'Please enter brand ID'
  },
  styleId: {
    type: Number,
    required: 'Please enter style ID'
  },
  carModel: {
    type: String,
    required: 'Please enter car model'
  },
  carColor:{
    type: String,
  },
  cylinder:{
    type: Number
  },
  fuelType: {
    type: [{
      type: String,
      enum: ['Gasoline', 'Diesel', 'Hybrid', 'Electric']
    }],
    required: 'Please enter fuelType',

  },
  transmitionType: {
    type: [{
        type: String,
        enum: ['Auto', 'Manual']
    }],
    required: 'Please enter the transmition type'
  },
  year: {
    type: Number,
  },
  extras: {
    type: [String]
  },
  passengerCapacity: {
    type: Number,
    required: 'Please enter passenger capacity'
  },
  dailyCost:{
    type: Number,
    required: 'Please enter daily rent cost'
  },
  carStatus: {
    type: [{
      type: String,
      enum: ['Available', 'Rented']
    }],
    default: ['Available']
  }
  
});

module.exports = mongoose.model('Cars', CarSchema);