'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var RentalSchema = new Schema({
  clientId: {
    type: String,
    required: 'Please enter an ID'
  },
  licensePlate: {
    type: String,
    required: 'Please enter car plate'
  },
  duration:{
    type: Number,
    required: 'Please enter duration of rental'
  },
  cost:{
    type: Number
  }
});

module.exports = mongoose.model('Rentals', RentalSchema);