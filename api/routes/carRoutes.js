'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/carController');

  // todoList Routes
  app.route('/cars')
    .get(todoList.list_all_cars)
    .post(todoList.create_a_car);




  app.route('/cars/:licensePlate')
    .get(todoList.read_a_car)
    .put(todoList.update_a_car)
    .delete(todoList.delete_a_car);
  app.route('/car/:brandId')
  	.get(todoList.read_cars_by_brand);
  app.route('/carsByPriceRange')
  	.post(todoList.read_cars_by_price_range);
  app.route('/carsByModel')
  	.post(todoList.read_cars_by_model);
};