'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/rentalController');

  // todoList Routes
  app.route('/rentals')
    .get(todoList.list_all_rentals)
    .post(todoList.create_a_rental);


  app.route('/rentals/:clientId')
    .get(todoList.read_a_rental)
    .put(todoList.update_a_rental)
    .delete(todoList.delete_a_rental);
};