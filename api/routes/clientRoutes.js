'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/clientController');

  // todoList Routes
  app.route('/clients')
    .get(todoList.list_all_clients)
    .post(todoList.create_a_client);


  app.route('/clients/:clientId')
    .get(todoList.read_a_client)
    .put(todoList.update_a_client)
    .delete(todoList.delete_a_client);
};