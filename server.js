var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/todoListModel'), //created model loading here
  Car = require('./api/models/carModel'),
  Client = require('./api/models/clientModel'),
  Rental = require('./api/models/rentalModel'),
  bodyParser = require('body-parser');
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route

var carRoutes = require('./api/routes/carRoutes'); //importing route
carRoutes(app); //register the route

var clientRoutes = require('./api/routes/clientRoutes'); //importing route
clientRoutes(app); //register the route

var rentalRoutes = require('./api/routes/rentalRoutes'); //importing route
rentalRoutes(app); //register the route

app.listen(port);


console.log('todo list RESTful API server started on: ' + port);